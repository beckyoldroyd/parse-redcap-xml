Parse xml in REDCAP format

Input:
  - xml in input dir

Output:
  - archivist_tables dir

Next step:
  - run [archivist_insert_pipeline](https://gitlab.com/jli755/archivist_insert_workaround) with files from archivist_tables directory
